﻿using UnityEditor;
using UnityEngine;

public static class EditorTools
{
    //% (ctrl), # (shift), & (alt)
    public const string TOOLS = "Tools/";
    public const string TOOLS_OTHER = TOOLS + "Other/";

    [MenuItem(TOOLS_OTHER + "ResetTransform &z")]
    static void ResetTransform()
    {
        foreach (Transform t in Selection.transforms)
        {
            t.localPosition = Vector3.zero;
            t.rotation = Quaternion.identity;
            t.localScale = Vector3.one;
        }
    }

    [MenuItem(TOOLS_OTHER + "Toggle active &d")]
    private static void ToggleActive()
    {
        foreach (GameObject go in Selection.gameObjects)
            go.SetActive(!go.activeSelf);
    }

    [MenuItem(TOOLS_OTHER + "Tools/Apply prefab &a")]
    static void ApplyPrefab()
    {
        var sel = Selection.activeGameObject;

        if (sel != null)
        {
            var parent = PrefabUtility.GetPrefabParent(sel);
            if (parent != null)
            {
                PrefabUtility.ReplacePrefab(PrefabUtility.FindRootGameObjectWithSameParentPrefab(sel), parent, ReplacePrefabOptions.ConnectToPrefab);
            }
        }
    }

}
