﻿using System;
using System.Collections.Generic;
using UnityEngine;

public static class UnityExtensions
{

    #region Float

    public static float Remap(this float value, float from1, float to1, float from2, float to2)
    {
        return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
    }

    public static Vector3 ToVector3(this float f)
    {
        return Vector3.one * f;
    }

    public static float RandomTo(this float f)
    {
        return UnityEngine.Random.Range(0, f);
    }

    #endregion

    #region Vector2

    public static float ToAngle(this Vector2 v2)
    {
        float ang = Vector2.Angle(v2, Vector2.right);
        Vector3 cross = Vector3.Cross(v2, Vector3.right);

        if (cross.z > 0)
            ang = 360 - ang;

        return ang;
    }

    public static Vector3 ToV3(this Vector2 v2)
    {
        return new Vector3(v2.x, v2.y, 0);
    }

    public static Vector2 Add(this Vector2 v2, float number)
    {
        return new Vector2(v2.x + number, v2.y + number);
    }

    public static Vector2 Add(this Vector2 v2, Vector2 other)
    {
        return new Vector2(v2.x + other.x, v2.y + other.y);
    }

    public static Vector2 Multiply(this Vector2 v, Vector3 other)
    {
        return new Vector2(v.x * other.x, v.y * other.y);
    }

    public static Vector2 SetX(this Vector2 v, float x)
    {
        return new Vector2(x, v.y);
    }

    public static Vector2 SetY(this Vector2 v, float y)
    {
        return new Vector2(v.x, y);
    }

    public static Vector2 Abs(this Vector2 a)
    {
        a.Set(Mathf.Abs(a.x), Mathf.Abs(a.y));
        return a;
    }

    public static Vector2 Round(this Vector2 v)
    {
        return new Vector3(Mathf.Round(v.x), Mathf.Round(v.y));
    }

    #endregion

    #region Vector3

    public static float ToAngle(this Vector3 v3)
    {
        float ang = Vector3.Angle(v3, Vector3.right);
        Vector3 cross = Vector3.Cross(v3, Vector3.right);

        if (cross.z > 0)
            ang = 360 - ang;

        return ang;
    }

    public static Vector2 ToV2(this Vector3 v3)
    {
        return new Vector2(v3.x, v3.y);
    }

    public static Vector3 Add(this Vector3 v3, float number)
    {
        v3.Set(v3.x + number, v3.y + number, v3.z + number);
        return v3;
    }
    public static Vector3 Add(this Vector3 v3, Vector3 other)
    {
        v3.Set(v3.x + other.x, v3.y + other.y, v3.z + other.z);
        return v3;
    }

    public static Vector3 Multiply(this Vector3 v3, Vector3 other)
    {
        v3.Set(v3.x * other.x, v3.y * other.y, v3.z * other.z);
        return v3;
    }

    public static Vector3 SetX(this Vector3 v3, float x)
    {
        v3.Set(x, v3.y, v3.z);
        return v3;
    }
    public static Vector3 SetY(this Vector3 v3, float y)
    {
        v3.Set(v3.x, y, v3.z);
        return v3;
    }
    public static Vector3 SetZ(this Vector3 v3, float z)
    {
        v3.Set(v3.x, v3.y, z);
        return v3;
    }

    public static Vector3 Abs(this Vector3 a)
    {
        a.Set(Mathf.Abs(a.x), Mathf.Abs(a.y), Mathf.Abs(a.z));
        return a;
    }

    public static Vector3 Round(this Vector3 v)
    {
        return new Vector3(Mathf.Round(v.x), Mathf.Round(v.y), Mathf.Round(v.z));
    }

    #endregion

    #region Bounds

    public static Vector3 RandomPoint(this Bounds bounds, string seed = "random")
    {
        if (seed == "random")
            seed = Time.realtimeSinceStartup.ToString();

        System.Random pseudoRandom = new System.Random(seed.GetHashCode());

        float x = pseudoRandom.Next((int) (bounds.min.x * 100), (int) (bounds.max.x * 100)) / 100f;
        float y = pseudoRandom.Next((int) (bounds.min.y * 100), (int) (bounds.max.y * 100)) / 100f;
        float z = pseudoRandom.Next((int) (bounds.min.z * 100), (int) (bounds.max.z * 100)) / 100f;
        return new Vector3(x, y, z);
    }

    public static Rect ToRect(this Bounds b)
    {
        Vector3 c = b.center;
        Vector3 e = b.extents;
        Vector3 s = b.size;
        return new Rect(c.x - e.x, c.y - e.y, s.x, s.y);
    }
    #endregion

    #region Rect

    public static Bounds ToBounds(this Rect r)
    {
        return new Bounds(r.center, r.size);
    }

    public static Rect WithHeight(this Rect r, float newH)
    {
        return new Rect(r.x, r.y, r.width, newH);
    }

    #endregion

    #region Transform

    /////////////////////////////////// POSITION
    public static void Move(this Transform t, Vector3 position, bool relative = false)
    {
        if (relative)
            t.position += position;
        else
            t.position = position;
    }

    public static void MoveX(this Transform t, float x, bool relative = false)
    {
        if (relative)
            t.position += new Vector3(x, 0, 0);
        else
            t.position = new Vector3(x, t.position.y, t.position.z);
    }

    public static void MoveY(this Transform t, float y, bool relative = false)
    {
        if (relative)
            t.position += new Vector3(0, y, 0);
        else
            t.position = new Vector3(t.position.x, y, t.position.z);
    }

    public static void MoveZ(this Transform t, float z, bool relative = false)
    {
        if (relative)
            t.position += new Vector3(0, 0, z);
        else
            t.position = new Vector3(t.position.x, t.position.y, z);
    }



    //////////////////////////////// LOCAL POSITION
    public static void LocalMove(this Transform t, Vector3 position, bool relative = false)
    {
        if (relative)
            t.localPosition += position;
        else
            t.localPosition = position;
    }

    public static void LocalMoveX(this Transform t, float x, bool relative = false)
    {
        if (relative)
            t.localPosition += new Vector3(x, 0, 0);
        else
            t.localPosition = new Vector3(x, t.localPosition.y, t.localPosition.z);
    }

    public static void LocalMoveY(this Transform t, float y, bool relative = false)
    {
        if (relative)
            t.localPosition += new Vector3(0, y, 0);
        else
            t.localPosition = new Vector3(t.localPosition.x, y, t.localPosition.z);
    }

    public static void LocalMoveZ(this Transform t, float z, bool relative = false)
    {
        if (relative)
            t.localPosition += new Vector3(0, 0, z);
        else
            t.localPosition = new Vector3(t.localPosition.x, t.localPosition.y, z);
    }


    //////////////////////////////// SCALE
    public static void Scale(this Transform t, Vector3 scale, bool relative = false)
    {
        if (relative)
            t.localScale += scale;
        else
            t.localScale = scale;
    }

    public static void Scale(this Transform t, float scale, bool relative = false)
    {
        if (relative)
            t.localScale += new Vector3(scale, scale, scale);
        else
            t.localScale = new Vector3(scale, scale, scale);
    }

    public static void ScaleX(this Transform t, float x, bool relative = false)
    {
        if (relative)
            t.localScale += new Vector3(x, 0, 0);
        else
            t.localScale = new Vector3(x, t.localScale.y, t.localScale.z);
    }

    public static void ScaleY(this Transform t, float y, bool relative = false)
    {
        if (relative)
            t.localScale += new Vector3(0, y, 0);
        else
            t.localScale = new Vector3(t.localScale.x, y, t.localScale.z);
    }

    public static void ScaleZ(this Transform t, float z, bool relative = false)
    {
        if (relative)
            t.localScale += new Vector3(0, 0, z);
        else
            t.localScale = new Vector3(t.localScale.x, t.localScale.y, z);
    }




    ///////////////////////////////// ROTATION
    public static void MyRotate(this Transform t, Vector3 rotation)
    {
        t.eulerAngles = new Vector3(rotation.x, rotation.y, rotation.z);
    }

    public static void RotateX(this Transform t, float x)
    {
        t.eulerAngles = new Vector3(x, t.localRotation.y, t.localRotation.z);
    }
    public static void RotateY(this Transform t, float y)
    {
        t.eulerAngles = new Vector3(t.localRotation.x, y, t.localRotation.z);
    }
    public static void RotateZ(this Transform t, float z)
    {
        t.eulerAngles = new Vector3(t.localRotation.x, t.localRotation.y, z);
    }





    public static IEnumerable<Transform> Childs(this Transform transform, bool recursive = false, bool includeInactive = false)
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            if (transform.GetChild(i).gameObject.activeSelf || includeInactive)
                yield return transform.GetChild(i);

            if (recursive)
                foreach (Transform childOfChild in transform.GetChild(i).Childs(true, includeInactive))
                    if (transform.GetChild(i).gameObject.activeSelf || includeInactive)
                        yield return childOfChild;
        }
    }

    #endregion

    #region SpriteRenderer

    #endregion

    #region Sprite
    public static Sprite GetCroppedSprite(this Sprite baseSprite, float xOffset, float yOffset, float width, float height, Vector2 pivot)
    {
        Texture2D baseTexture = baseSprite.texture;
        Rect textureRect = new Rect(baseSprite.textureRect.position.x + xOffset, baseSprite.textureRect.position.y + yOffset, width, height);
        Sprite newSprite = Sprite.Create(baseTexture, textureRect, pivot, 16);

        return newSprite;
    }
    #endregion

    #region Camera

    public static Bounds OrthographicBounds(this Camera cam)
    {
        return cam.orthographicBounds(transform => transform.position);
    }

    public static Bounds LocalOrthographicBounds(this Camera cam)
    {
        return cam.orthographicBounds(transform => transform.localPosition);
    }

    private static Bounds orthographicBounds(this Camera cam, Func<Transform, Vector3> positionGetter)
    {
        float height = cam.orthographicSize * 2;
        return new Bounds(positionGetter(cam.transform), new Vector2(height * Screen.width / Screen.height, height));
    }

    #endregion

    #region List
    public static bool InBounds<T>(this IList<T> list, int i)
    {
        return i > -1 && i < list.Count;
    }
    #endregion

    #region Misc

    public static Color WithAlpha(this Color color, float a)
    {
        return new Color(color.r, color.g, color.b, a);
    }

    public static Texture2D toTexture(this Sprite sprite)
    {
        if (sprite.rect.width != sprite.texture.width)
        {
            Texture2D newText = new Texture2D((int) sprite.rect.width, (int) sprite.rect.height);
            Color[] newColors = sprite.texture.GetPixels((int) sprite.textureRect.x,
                                                         (int) sprite.textureRect.y,
                                                         (int) sprite.textureRect.width,
                                                         (int) sprite.textureRect.height);
            newText.SetPixels(newColors);
            newText.Apply();
            return newText;
        }
        else
            return sprite.texture;
    }

    #endregion

}