﻿using DG.Tweening;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Test : ExtendedBehaviour
{

    public Text text;
    public string levelName;

    private AsyncOperation async;

    void Start()
    {
        StartCoroutine(Load());
    }

    private IEnumerator Load()
    {
        //yield return new WaitForSeconds(2f);

        async = SceneManager.LoadSceneAsync(levelName);
        async.allowSceneActivation = false;

        yield return async.isDone;

        //yield return new WaitForSeconds(2f);

        text.material.DOColor(Color.white.WithAlpha(0), 1f).OnComplete(() =>
        {
            async.allowSceneActivation = true;
            print(Time.realtimeSinceStartup);
        });
    }
}
