﻿public enum Events
{
    OnGameOver,
    OnCoinCollected,
    OnDragBegin,
    OnDrag,
    OnDragEnd,
    OnAimEnd,
    OnGoatDestroyed,
    OnGoatBallooned,
    OnGirlMilked,
    OnScoreChanged,
    OnHealthChanged,
    OnGoatMissed,
}