﻿using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

public class Messenger
{

    public static bool debug;
    private static readonly Dictionary<string, GameEvent> eventsDictionary = new Dictionary<string, GameEvent>();

    private static BindingFlags bingingFlags = BindingFlags.Public | BindingFlags.Static | BindingFlags.NonPublic |
                                               BindingFlags.Instance | BindingFlags.FlattenHierarchy;


    public static void Register(string eventName, Action action, bool playThenDisabled = false)
    {
        // Если в таблице нет ключа, создаем его
        if (!eventsDictionary.ContainsKey(eventName))
        {
            GameEvent value = new GameEvent {eventName = eventName};
            eventsDictionary.Add(eventName, value);
        }

        // После регистрируем
        var gameEvent = eventsDictionary[eventName];
        gameEvent.RegisterHandler(action.Target, action.Method, playThenDisabled);
    }

    public static void Register(Events eventName, Action action, bool playThenDisabled = false)
    {
        Register(eventName.ToString(), action, playThenDisabled);
    }


    public static void Unregister(string eventName, Action action)
    {
        if (!eventsDictionary.ContainsKey(eventName)) return;

        var gameEvent = eventsDictionary[eventName];
        gameEvent.UnregiserHandler(action.Target, action.Method);
    }

    public static void Unregister(Events eventName, Action action)
    {
        Unregister(eventName.ToString(), action);
    }


    // вызвать событие
    public static void Invoke(string eventName, params object[] args)
    {
        if (debug) Debug.Log("Messenger: " + eventName);
        if (!eventsDictionary.ContainsKey(eventName)) return;

        var gameEvent = eventsDictionary[eventName];
        gameEvent.Invoke(args);
    }

    // вызвать событие
    public static void Invoke(Events eventName, params object[] args)
    {
        Invoke(eventName.ToString(), args);
    }

    // зарегистрировать все помеченные методы
    public static void RegisterAll(object handler)
    {
        // получаем все методы из данного экземляра класса
        var methods = handler.GetType().GetMethods(bingingFlags);
        // бежим по ним
        foreach (var mi in methods)
        {
            Handler[] handlers;
            // если метод содержит наш атрибут запоминаем его
            if ((handlers = (Handler[]) mi.GetCustomAttributes(typeof(Handler), true)).Length != 0)
            {

                foreach (var handlerAttribute in handlers)
                {
                    var eventName = handlerAttribute.EventName;

                    if (!eventsDictionary.ContainsKey(eventName))
                    {
                        GameEvent value = new GameEvent {eventName = eventName};
                        eventsDictionary.Add(eventName, value);
                    }

                    // После регистрируем
                    var gameEvent = eventsDictionary[eventName];
                    gameEvent.RegisterHandler(handler, mi, handlerAttribute.PlayThenDisabled);
                }
            }
        }
    }


    private class GameEvent
    {
        public string eventName;
        private readonly List<EventHandlerData> eventHandlers = new List<EventHandlerData>();

        public void Invoke(object[] args)
        {
            for (var i = 0; i < eventHandlers.Count; i++)
            {
                var eventHandlerData = eventHandlers[i];
                var unityObject = (MonoBehaviour) eventHandlerData.handle;

                if (unityObject != null && !unityObject.Equals(null))
                {
                    if (unityObject.gameObject.activeSelf || eventHandlerData.playThenDisabled)
                    {
                        var method = eventHandlerData.method;
                        var parameters = method.GetParameters();
                        if (args.Length != parameters.Length) // проверка на валидность типов
                        {
                            Debug.LogError("Type mismatch! Event: " + eventName + " Handler: " + eventHandlerData.handle +
                                           " GameObject Listener: " + unityObject.name);
                            break;
                        }

                        method.Invoke(eventHandlerData.handle, args);
                    }
                }
                else
                {
                    eventHandlers.Remove(eventHandlers[i]);
                    i--; //так как при удалении все элементы сместятся на место удаленного
                }
            }
        }

        public void RegisterHandler(object handler, MethodInfo method, bool playThenDisabled = false)
        {
            foreach (var eventHandler in eventHandlers)
            {
                if (eventHandler.handle == handler && eventHandler.method == method) return;
            }

            eventHandlers.Add(new EventHandlerData
            {
                handle = handler,
                method = method,
                playThenDisabled = playThenDisabled
            });
        }

        public void UnregiserHandler(object handler, MethodInfo method)
        {
            foreach (var eventHandler in eventHandlers)
            {
                if (eventHandler.handle != handler || eventHandler.method != method) continue;
                eventHandlers.Remove(eventHandler);
                return;
            }
        }

        private class EventHandlerData
        {
            public bool playThenDisabled;
            public object handle;
            public MethodInfo method;
        }
    }

    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
    public class Handler : Attribute
    {
        public Handler(string eventNameName, bool playThenDisabled = false)
        {
            EventName = eventNameName;
            PlayThenDisabled = playThenDisabled;
        }

        public Handler(Events eventNameName, bool playThenDisabled = false)
        {
            EventName = eventNameName.ToString();
            PlayThenDisabled = playThenDisabled;
        }

        public string EventName { get; set; }
        public bool PlayThenDisabled { get; set; }
    }
}