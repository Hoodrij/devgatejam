﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class TimerAggregator : SingletonForTimer<TimerAggregator>
{

    private static List<Timer> activeTimers = new List<Timer>();

    void Awake()
    {
        DontDestroyOnLoad(this);
    }

    void Update()
    {
        for (int i = 0; i < activeTimers.Count; i++)
        {
            activeTimers[i].MyUpdate();
        }
    }

    public static Timer Start(float time, Action callback)
    {
        return Instance.start(time, 0, false, callback);
    }

    public static Timer Start(float time, float delay, Action callback)
    {
        return Instance.start(time, delay, false, callback);
    }

    public static Timer Start(float time, float delay, bool repeat, Action callback)
    {
        return Instance.start(time, delay, repeat, callback);
    }

    public Timer start(float time, float delay, bool repeat, Action callback)
    {
        if (time < 0) time = 0;
        Timer nTimer = new Timer();
        nTimer.Init(time, delay, repeat, callback);

        return nTimer;
    }

    public static void Stop(Timer timer)
    {
        if (timer == null) return;
        for (int i = 0; i < activeTimers.Count; i++)
        {
            if (timer != activeTimers[i]) continue;

            timer.Stop();
            return;
        }
    }

    public static void Pause(Timer timer)
    {
        if (timer == null) return;
        for (int i = 0; i < activeTimers.Count; i++)
        {
            if (timer != activeTimers[i]) continue;

            timer.Pause();
            return;
        }
    }

    public static void Resume(Timer timer)
    {
        if (timer == null) return;
        for (int i = 0; i < activeTimers.Count; i++)
        {
            if (timer != activeTimers[i]) continue;

            timer.Resume();
            return;
        }
    }

    public class Timer
    {
        private float time;
        private bool repeat;
        public Action callback;

        public float nextCallbackTime;

        public bool isWorking;
        public bool isPaused;


        public void Init(float time)
        {
            Init(time, 0, false, callback);
        }

        public void Init(float time, Action callback)
        {
            Init(time, 0, false, callback);
        }

        public void Init(float time, float delay, bool repeat, Action callback)
        {
            this.time = time;
            this.repeat = repeat;
            this.callback = callback;

            nextCallbackTime = time + delay + Time.time;

            isWorking = true;
            isPaused = false;

            activeTimers.Add(this);
        }

        public void AddTime(float time)
        {
            nextCallbackTime += time;
        }

        public Timer SetTime(float time)
        {
            if (time < 0) time = 0;
            nextCallbackTime = time + Time.time;
            return this;
        }

        public Timer SetCallBack(Action callback)
        {
            this.callback = callback;
            return this;
        }

        public void MyUpdate()
        {
            if (isPaused)
            {
                nextCallbackTime += Time.deltaTime;
                return;
            }

            if (nextCallbackTime - Time.time <= 0)
            {
                if (repeat) nextCallbackTime += time;
                else Stop();

                if (callback != null)
                    callback.Invoke();
            }
        }

        public float GetTimeLeft()
        {
            return nextCallbackTime - Time.time < 0 ? 0 : nextCallbackTime - Time.time;
        }

        public void Complete()
        {
            nextCallbackTime = 0;
        }

        public void Pause()
        {
            isPaused = true;
        }

        public void Resume()
        {
            isPaused = false;
        }

        public void Stop()
        {
            if (!isWorking) return;
            isWorking = false;
            activeTimers.Remove(this);
        }
    }
}


