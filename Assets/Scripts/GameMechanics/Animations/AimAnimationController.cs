﻿using DG.Tweening;
using UnityEngine;

public class AimAnimationController : ExtendedBehaviour
{
    public SpriteRenderer startPointImage;
    public SpriteRenderer endPointImage;
    public LineRenderer lineRenderer;

    public float maxLenght;

    private Vector3 startTouchPos;
    private Vector3 touchPos;

    private float width;
    private Tweener widthTween;

    void Start()
    {
        width = lineRenderer.widthMultiplier;
        lineRenderer.widthMultiplier = 0;
        startPointImage.color = Color.white.WithAlpha(0);
        endPointImage.color = Color.white.WithAlpha(0);

        Messenger.RegisterAll(this);
    }

    [Messenger.Handler(Events.OnDragBegin)]
    void OnAimBegin(Vector3 pos)
    {
        startTouchPos = pos;
        startPointImage.transform.position = pos + Vector3.up * 0.1f;
        lineRenderer.positionCount = 2;
        lineRenderer.SetPosition(0, startPointImage.transform.position);
        lineRenderer.SetPosition(1, startPointImage.transform.position);

        startPointImage.DOKill();
        startPointImage.DOFade(1, 0.1f);
        endPointImage.DOKill();
        endPointImage.DOFade(1, 0.1f);
        widthTween.Kill();
        widthTween = DOTween.To(() => lineRenderer.widthMultiplier, x => lineRenderer.widthMultiplier = x, width, 0.5f).SetDelay(0.1f);
    }

    [Messenger.Handler(Events.OnDrag)]
    void OnAim(Vector3 pos)
    {
        touchPos = pos;
        var dir = -(startTouchPos - touchPos).normalized;
        float distance = Mathf.Pow(Vector3.Distance(startTouchPos, touchPos), 0.6f);
        var endPos = startTouchPos + dir * Mathf.Min(distance, maxLenght);

        endPointImage.transform.position = endPos + Vector3.up * 0.1f;
        lineRenderer.SetPosition(1, endPointImage.transform.position);

        Messenger.Invoke(Events.OnGoatBallooned, Mathf.Clamp01(distance / maxLenght));
    }

    [Messenger.Handler(Events.OnDragEnd)]
    void OnAimEnd(Vector3 pos)
    {
        touchPos = pos;
        var dir = -(startTouchPos - touchPos).normalized;
        float distance = Mathf.Pow(Vector3.Distance(startTouchPos, touchPos), 0.6f);

        DOTween.To(() => endPointImage.transform.position, v => endPointImage.transform.position = v, startTouchPos, 0.7f)
            .OnUpdate(() =>
            {
                lineRenderer.SetPosition(1, endPointImage.transform.position);
            })
            .SetEase(Ease.OutElastic);

        startPointImage.DOKill();
        startPointImage.DOFade(0, 0.5f).SetDelay(0.2f);
        endPointImage.DOKill();
        endPointImage.DOFade(0, 0.5f).SetDelay(0.2f);
        widthTween.Kill();
        widthTween = DOTween.To(() => lineRenderer.widthMultiplier, x => lineRenderer.widthMultiplier = x, 0, 0.5f).SetDelay(0.2f);

        Messenger.Invoke(Events.OnAimEnd, -dir, Mathf.Clamp01(distance / maxLenght));
    }
}
