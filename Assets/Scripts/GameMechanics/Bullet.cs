﻿using UnityEngine;

public class Bullet : ExtendedBehaviour
{

    void Start()
    {
        Messenger.RegisterAll(this);

    }

    [Messenger.Handler(Events.OnGameOver)]
    private void Pew(Vector2 dir)
    {
        print("Shoot direction : " + dir);
    }

}
