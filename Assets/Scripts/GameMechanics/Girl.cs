﻿using DG.Tweening;
using UnityEngine;

public class Girl : ExtendedBehaviour
{

    public float colliderZCoord;


    void Start()
    {

    }

    void Update()
    {

    }

    public void Init(int i)
    {
        int sign = i % 2 == 0 ? 1 : -1;
        transform.Scale(Random.Range(transform.localScale.y * 0.95f, transform.localScale.y * 1.05f));
        transform.Rotate(Vector3.back, sign * 17);
        transform.DORotate(Vector3.back * sign * -34, Random.Range(4f, 5f)).SetRelative(true).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.InOutQuint).SetDelay(i * Random.Range(0.8f, 1.2f));
    }

    public void OnHit()
    {
        Vector3 iniScale = transform.localScale;
        DOTween.Sequence()
            .Append(transform.DOScale(transform.localScale * 0.95f, 0.1f).SetEase(Ease.InSine))
            .Append(transform.DOScale(iniScale, 0.5f).SetEase(Ease.OutElastic))
            ;
        // TODO Play sound
    }
}
