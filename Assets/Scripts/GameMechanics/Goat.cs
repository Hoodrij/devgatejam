﻿using DG.Tweening;
using UnityEngine;

public class Goat : ExtendedBehaviour
{
    public Transform model;

    public float velocity;
    public float maxScale;
    private Vector3 speed;

    private bool isActive;

    private TimerAggregator.Timer destroyTimer;

    void Start()
    {
        Messenger.RegisterAll(this);
    }

    public void Init(Vector3 pos)
    {
        if (destroyTimer != null)
            destroyTimer.Stop();
        transform.DOKill();
        transform.position = pos;
        transform.localScale = Vector3.zero;

        model.transform.DOKill();
        model.localPosition = Vector3.zero;
        //model.rotation = Quaternion.identity;
        model.localScale = Vector3.one;

        speed = Vector3.zero;

        transform.DOScale(1, 1.5f);

        isActive = true;
    }

    void Update()
    {
        transform.position = transform.position + speed;
    }

    [Messenger.Handler(Events.OnAimEnd)]
    void Fire(Vector3 dir, float power)
    {
        dir.y = 0;
        speed = velocity * dir * power;

        model.DOShakePosition(10, 1.2f, 7, 90f, false, false);
        model.DOShakeRotation(10, 90, 15, 90, false);
        model.DOScale(1, 0.5f);

        destroyTimer = TimerAggregator.Start(5, OnDestroyAction);
    }

    // При натяжении резинки
    [Messenger.Handler(Events.OnGoatBallooned)]
    void OnBalloonGoat(float value)
    {
        model.transform.localScale = Vector3.one * Mathf.Clamp(1 + value, 1, 2);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "KillZone")
        {
            Messenger.Invoke(Events.OnGoatMissed);
            OnDestroyAction();
        }

        if (!isActive) return;
        if (other.gameObject.tag == "Enemy")
        {
            isActive = false;
            Messenger.Invoke(Events.OnGirlMilked);
            SplashImpact splash = resources.splashPool.Pop();
            splash.Init(transform.position);
            other.GetComponentInParent<Girl>().OnHit();
            OnDestroyAction();
        }
    }

    private void OnDestroyAction()
    {
        resources.goatPool.Push(this);
        Messenger.Invoke(Events.OnGoatDestroyed);
    }


}
