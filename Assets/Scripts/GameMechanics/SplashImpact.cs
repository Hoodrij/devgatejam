﻿using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;

public class SplashImpact : ExtendedBehaviour
{

    public SpriteRenderer textImage;
    public SpriteRenderer bigSplashImage;
    public SpriteRenderer smallSplashImage;

    public List<SpriteRenderer> canvasGroup;

    public List<Sprite> textImages;

    private Vector3 iniScale;

    public void Init(Vector3 pos)
    {
        iniScale = transform.localScale;

        textImage.sprite = textImages[Random.Range(0, textImages.Count)];
        bigSplashImage.transform.Rotate(Vector3.back, Random.Range(0, 180));
        smallSplashImage.transform.Rotate(Vector3.back, Random.Range(0, 180));

        transform.DOKill();
        transform.Scale(iniScale * 0.65f);
        transform.DOScale(iniScale, 0.2f).SetEase(Ease.OutBack);
        transform.position = pos;

        canvasGroup.ForEach(s =>
        {
            s.DOKill();
            s.color = s.color.WithAlpha(0);
            s.DOFade(1, 0.1f).OnComplete(() =>
            {
                s.DOFade(0, 2).SetDelay(0.5f);
            });
        });
    }
}
