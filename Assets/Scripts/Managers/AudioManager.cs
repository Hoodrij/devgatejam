﻿using UnityEngine;

public class AudioManager : ExtendedBehaviour
{

    public AudioSource audioSource;

    void Start()
    {
        Messenger.RegisterAll(this);
    }

    [Messenger.Handler(Events.OnAimEnd)]
    private void StartMusic(Vector3 dir, float power)
    {
        if (audioSource.isPlaying) return;
        audioSource.Play();
    }


    void Update()
    {

    }
}
