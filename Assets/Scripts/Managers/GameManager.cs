﻿using UnityEngine;

public enum InputType
{
    Mobile,
    PC
}

[DefaultExecutionOrder(-1000)]
public class GameManager : PersistentSingleton<GameManager>
{
    public bool isPublicBuild = true;

    public GameObject[] toActivate;

    protected override void Init()
    {
        if (FindObjectsOfType<GameManager>().Length > 1)
        {
            DestroyImmediate(gameObject);
        }

        foreach (GameObject o in toActivate)
        {
            if (o != null)
                o.SetActive(true);
        }

        if (isPublicBuild)
        {

        }

        if (true/*!file.exists()*/) FirstInit();
    }

    private void FirstInit()
    {

    }
}
