﻿using DG.Tweening;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneHandler : PersistentSingleton<SceneHandler>
{
    [SerializeField]
    private Image splashImage;

    private Scenes nextScene;
    private AsyncOperation async;


    public void SetSceneInsta(Scenes scene)
    {
        SceneManager.LoadScene(scene.ToString());
    }

    public void SetScene(Scenes scene)
    {
        nextScene = scene;
        StartCoroutine(Load());
    }


    private IEnumerator Load()
    {
        async = SceneManager.LoadSceneAsync(nextScene.ToString());
        async.allowSceneActivation = false;

        yield return async.isDone;

        ShowSwitchAnimation();
    }

    private void ShowSwitchAnimation()
    {
        DOTween.Sequence()
            .Append(splashImage.DOFade(1, 0.07f)
                .OnComplete(SwitchScene))
            .Append(splashImage.DOFade(0, 0.4f))
            ;
    }

    private void SwitchScene()
    {
        async.allowSceneActivation = true;
    }
}
