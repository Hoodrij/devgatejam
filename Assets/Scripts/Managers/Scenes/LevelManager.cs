﻿using System.Collections.Generic;
using UnityEngine;

public class LevelManager : ExtendedBehaviour
{
    public Transform spawnPoint;
    public Transform girlsContainer;
    public Girl girlPrefab;
    public int girlsCount;

    [Space]
    public bool isGameOver;

    public int score;
    public int health;

    private List<Girl> girlsList = new List<Girl>();

    void Start()
    {
        isGameOver = false;

        SpawnNewGoat();

        for (int i = 0; i < girlsCount; i++)
        {
            Girl girl = Instantiate(girlPrefab);
            girl.Init(i);
            girl.transform.parent = girlsContainer;
            girl.transform.localPosition = Vector3.zero;
            girlsList.Add(girl);
        }

        Messenger.RegisterAll(this);
    }

    [Messenger.Handler(Events.OnGoatDestroyed)]
    private void SpawnNewGoat()
    {
        if (isGameOver) return;
        var goat = resources.goatPool.Pop();
        goat.Init(spawnPoint.position);
    }

    [Messenger.Handler(Events.OnGameOver)]
    public void OnGameOver()
    {
        isGameOver = true;
    }

    [Messenger.Handler(Events.OnGirlMilked)]
    public void UpdateScore()
    {
        score++;
        Messenger.Invoke(Events.OnScoreChanged, score);
    }

    [Messenger.Handler(Events.OnGoatMissed)]
    private void UpdateHealth()
    {
        health--;
        Messenger.Invoke(Events.OnHealthChanged, health);
        if (health <= 0)
            Messenger.Invoke(Events.OnGameOver);
    }

    public void RestartButtonClick()
    {
        SceneHandler.Instance.SetScene(Scenes.GameScene);
    }
}
