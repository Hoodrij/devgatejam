﻿public class LoadingSceneManager : ExtendedBehaviour
{

    void Start()
    {
        TimerAggregator.Start(1, () =>
        {
            SceneHandler.Instance.SetScene(Scenes.MenuScene);
        });
    }


}
