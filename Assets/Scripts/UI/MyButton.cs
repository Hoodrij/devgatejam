﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class MyButton : ExtendedBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler,
    IPointerClickHandler
{
    public bool withAnimation = true;

    public UnityEvent onMouseDown;
    public UnityEvent onMouseUp;
    public UnityEvent onMouseClick;

    private bool isPointerInsideButton;

    public void OnPointerDown(PointerEventData eventData)
    {
        isPointerInsideButton = true;

        onMouseDown.Invoke();

        StartAnim();
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (isPointerInsideButton)
            onMouseUp.Invoke();

        EndAnim();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        onMouseClick.Invoke();
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        isPointerInsideButton = false;
        EndAnim();
    }

    private void StartAnim()
    {
        if (!withAnimation) return;

        transform.DOKill();
        transform.DOScale(new Vector3(0.85f, 1.15f, 1), 0.09f).SetEase(Ease.OutCirc);
    }

    private void EndAnim()
    {
        if (!withAnimation) return;

        transform.DOKill();
        transform.DOScale(1, 0.7f).SetEase(Ease.OutElastic, 0.5f);
    }
}
