﻿using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIUpdater : ExtendedBehaviour
{
    public Text scoreText;
    private Tween scoreTextSeq;

    public RectTransform restartButton;

    public List<Transform> bottlesList;

    public Image[] allUIImages;
    public Text[] allUITexts;


    void Awake()
    {
        UpdateScore(0);
        scoreText.transform.Scale(0);
        Messenger.RegisterAll(this);
    }

    [Messenger.Handler(Events.OnScoreChanged)]
    private void UpdateScore(int score)
    {
        scoreText.text = score.ToString();

        if (score == 0) return;
        scoreTextSeq.Kill();
        scoreTextSeq = DOTween.Sequence()
            .Append(scoreText.transform.DOScale(1.6f, 0.1f))
            .Append(scoreText.transform.DOScale(1f, 0.1f))
            ;
    }

    [Messenger.Handler(Events.OnHealthChanged)]
    private void UpdateHealth(int health)
    {
        if (health < bottlesList.Count)
        {
            var item = bottlesList[0];
            bottlesList.RemoveAt(0);
            Destroy(item.gameObject);
        }
    }

    [Messenger.Handler(Events.OnGameOver)]
    private void ShowGameOver()
    {
        restartButton.DOKill();
        restartButton.DOScale(1, 0.7f).SetEase(Ease.OutElastic);
    }



    public void SetShowUI(bool isShown, float time = 0)
    {
        foreach (Image image in allUIImages)
        {
            image.DOKill();
            image.material.DOFade(isShown ? 1 : 0, time).SetUpdate(UpdateType.Normal).SetEase(Ease.InOutQuad);
        }
        foreach (Text text in allUITexts)
        {
            text.DOKill();
            text.material.DOFade(isShown ? 1 : 0, time).SetUpdate(UpdateType.Normal).SetEase(Ease.InOutQuad);
        }
    }
}
