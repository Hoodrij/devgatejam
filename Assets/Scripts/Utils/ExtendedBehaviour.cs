﻿using UnityEngine;

public class ExtendedBehaviour : MonoBehaviour
{

    [HideInInspector]
    public GameManager gameManager { get { return GameManager.Instance; } }
    [HideInInspector]
    public RuntimeResources resources { get { return RuntimeResources.Instance; } }

    public float random01()
    {
        return Random.Range(0f, 1f);
    }

    public bool randomBool()
    {
        return Random.Range(0f, 1f) > 0.5f;
    }
}
