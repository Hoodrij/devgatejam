﻿using UnityEngine;
using UnityEngine.EventSystems;

public class InputHandler : ExtendedBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    public LayerMask layersToHit;
    private RaycastHit hit = new RaycastHit();

    private Vector3 startTouchPos;
    private Vector3 touchPos;
    private bool isTouching;

    public void OnBeginDrag(PointerEventData eventData)
    {
        Ray ray = Camera.main.ScreenPointToRay(eventData.position);
        if (!Physics.Raycast(ray.origin, ray.direction, out hit, layersToHit)) return;

        startTouchPos = hit.point;
        isTouching = true;
        Messenger.Invoke(Events.OnDragBegin, startTouchPos);
    }

    public void OnDrag(PointerEventData eventData)
    {
        Ray ray = Camera.main.ScreenPointToRay(eventData.position);
        if (!Physics.Raycast(ray.origin, ray.direction, out hit, layersToHit)) return;

        touchPos = hit.point;

        Messenger.Invoke(Events.OnDrag, touchPos);
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        Ray ray = Camera.main.ScreenPointToRay(eventData.position);
        if (!Physics.Raycast(ray.origin, ray.direction, out hit, layersToHit)) return;

        touchPos = hit.point;
        isTouching = false;

        Messenger.Invoke(Events.OnDragEnd, touchPos);
    }
}
