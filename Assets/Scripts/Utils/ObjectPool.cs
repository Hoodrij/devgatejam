﻿using System.Collections.Generic;
using UnityEngine;

public class ObjectPool<T> where T : Component
{
    Stack<T> items;
    GameObject parentGO = null;
    Transform parentGOTransform = null;
    T originalGO;

    int inUse = 0;

    public int Capacity { get { return inUse + items.Count; } }
    public int InUse { get { return inUse; } }

    public ObjectPool(string name, T go, int capacity)
    {
        if (name != null)
        {
            parentGO = new GameObject(name);
            parentGOTransform = parentGO.transform;
            parentGOTransform.SetParent(RuntimeResources.Instance.commonPoolsParent.transform);
            //GameObject.DontDestroyOnLoad(parentGO);
        }


        Init(go, capacity);
    }

    public ObjectPool(Transform parent, T go, int capacity)
    {
        if (parent != null)
        {
            parentGO = parent.gameObject;
            parentGOTransform = parentGO.transform;
            //GameObject.DontDestroyOnLoad(parentGO);
        }

        Init(go, capacity);
    }

    private void Init(T go, int capacity)
    {
        items = new Stack<T>(capacity);
        originalGO = go;


        T instance = null;
        Vector3 position = go.transform.position;
        Quaternion rotation = go.transform.rotation;
        Vector3 scale = go.transform.localScale;

        for (int i = 0; i < capacity; ++i)
        {
            instance = (T) MonoBehaviour.Instantiate(go, position, rotation);
#if UNITY_EDITOR
            instance.name = go.name + i;
#endif
            instance.gameObject.SetActive(false);
            instance.transform.localScale = scale;
            instance.transform.SetParent(parentGOTransform);
            items.Push(instance);
        }
    }

    #region Push item

    public virtual void Push(T item)
    {
        if (!items.Contains(item))
        {
#if DEBUG
            if (!(item.gameObject == null || item.gameObject.activeSelf))
                Debug.LogError("Tried to pool inactive object. Ignoring...Check for duplicate return to pool" + parentGO.name);

            if (inUse < 1)
                Debug.LogError("Tried to pool object while pool had no items in use. Pool: " + parentGO.name);
#endif
            item.gameObject.SetActive(false);
            items.Push(item);
            inUse--;

        }
    }

    public virtual void PushReparent(T item)
    {
        Push(item);
        item.transform.parent = parentGOTransform;
    }

    public virtual void PushAll()
    {
        throw new System.NotImplementedException("ObjectPool does not implement PushAll method");
    }

    #endregion

    #region Pop item

    public virtual T Pop(bool makeActive = true)
    {
        if (items.Count == 0)
            AddInstance();

        T item = items.Pop();
        if (makeActive)
            item.gameObject.SetActive(true);
        inUse++;
        return item;
    }

    #endregion

    void AddInstance()
    {
        Transform originalTr = originalGO.transform;

        T instance = (T) MonoBehaviour.Instantiate(originalGO, originalTr.position, originalTr.rotation);
#if UNITY_EDITOR
        instance.name = originalGO.name + Capacity;
#endif
        instance.gameObject.SetActive(false);
        instance.transform.localScale = originalTr.localScale;
        instance.transform.parent = parentGOTransform;
        items.Push(instance);
    }
}
