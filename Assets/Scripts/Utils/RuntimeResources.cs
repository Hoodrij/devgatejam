﻿using UnityEngine;

public class RuntimeResources : Singleton<RuntimeResources>
{
    public LevelManager levelManager;

    [Header("Pools")]
    public int goatsPoolCap;
    public Goat goatPrefab;
    public ObjectPool<Goat> goatPool;
    [Space]
    public int splashesPoolCap;
    public SplashImpact splashPrefab;
    public ObjectPool<SplashImpact> splashPool;

    [Header("UI")]
    public UIUpdater uiUpdater;

    [HideInInspector]
    public GameObject commonPoolsParent;

    void Awake()
    {

    }

    void Start()
    {
        commonPoolsParent = GameObject.Find("Pools") ?? new GameObject("Pools");

        goatPool = new ObjectPool<Goat>("GoatPool", goatPrefab, goatsPoolCap);
        splashPool = new ObjectPool<SplashImpact>("SplashPool", splashPrefab, splashesPoolCap);
    }
}
