﻿using UnityEngine;

[System.Serializable]
public class IntSpriteDict : KVPList<int, Sprite> { }
