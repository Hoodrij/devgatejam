﻿using System;
using UnityEngine;

public class Singleton<T> : ExtendedBehaviour where T : Component
{
    private static T instance;
    private static object _lock = new object();


    public static T Instance
    {
        get
        {
            lock (_lock)
            {
                if (instance == null)
                    CreateInstanceIfNone(true);
                return instance;
            }
        }
    }

    protected void Awake()
    {
        instance = this as T;
        init();
    }

    protected virtual void init()
    {
    }

    public static void CreateInstanceIfNone(bool warn = false)
    {
        if (instance == null)
        {
            var instances = FindObjectsOfType<T>();

            var length = instances.Length;
            if (length > 0)
            {
                instance = instances[0];

                if (length > 1)
                {
                    Debug.LogError("more that one singleton " + instance.GetType().Name);
                    //DebugUtils.LogCollectionWithContexts(instances, "more than one singleton in a scene!");
                }

                return;
            }

            CreateInstance(); // awake will be called automaticly
            if (warn)
                Debug.LogWarning("instance was null and created " + instance.name, instance);
        }
    }

    public static T CreateInstance()
    {
        return new GameObject(typeof(T).Name).AddComponent<T>();
    }

    public static bool HasInstance()
    {
        return instance != null;
    }

    public static void InvokeOnInstanceIfHasOne(Action<T> action, string warning = "")
    {
        if (HasInstance())
            action(Instance);
        else Debug.LogWarning("InvokeOnInstanceIfHasOne() failed " + typeof(T) + ": " + warning);
    }

    /// nonpersistent singleton is destroyed between scenes and when application quits
    public virtual void OnDestroy() { }
}







public class PersistentSingleton<T> : Singleton<T> where T : Component
{
    protected sealed override void init()
    {
        DontDestroyOnLoad(gameObject);
        Init();
    }

    protected virtual void Init()
    {
    }
}

